#include "data.h"

uint8_t my_itoa(int32_t data, uint8_t * ptr, uint32_t base)
{
	uint8_t str[33];
	uint8_t i, k = 0;
	if (data < 0)
	{
		data = -data;
		*(ptr) = '-';
		k++;
	}
	// invariant: we have written i chars to str
	for(i = 0; data; i++)
	{
		uint8_t r = data%base;
		if(r < 10) *(str+i) = '0' + r;
		else *(str+i) = 'A' + r-10;
		data/=base;
	}
	while(i) *(ptr+k++) = *(str+--i);
	*(ptr+k++) = '\0';
	return k;
}

int32_t my_atoi(uint8_t * ptr, uint8_t digits, uint32_t base)
{
	int32_t ret =0, num;
	int8_t i=0, k, sign =1;
	if(*ptr == '-')
	{
		sign = -1;
		i++;
		digits--;
	}
	while(digits!=1)
	{
		num = *(ptr+i) <= '9' ? (int32_t) *(ptr+i) - '0' : (int32_t) *(ptr+i) - 'A'+10;
		for(k=1;k!=(digits-1);k++) num*=base;
		ret+=num;
		i++;
		digits--;
	}
	return ret*sign;
}
