/******************************************************************************
 * Copyright (C) 2017 by Alex Fosdick - University of Colorado
 *
 * Redistribution, modification or use of this software in source or binary
 * forms is permitted as long as the files maintain this copyright. Users are 
 * permitted to modify this and use it to learn about the field of embedded
 * software. Alex Fosdick and the University of Colorado are not liable for any
 * misuse of this material. 
 *
 *****************************************************************************/
/**
 * @file stats.c
 * @brief Week 1 Application Assignment
 *
 * functions to perform some math on some data
 *
 * @author Lucas Pirola
 * @date 22/Feb/2018
 *
 */



#include <stdio.h>
#include "stats.h"
#include "platform.h"

/* Size of the Data Set */
#define SIZE (40)


unsigned char find_minimum(unsigned char *a, const unsigned int sz) {
	int i;
  unsigned char min = a[0];
	
  if( sz < 2) return min;
  
	for (i = 1; i != sz; i++)
		if (a[i] < min)
			min = a[i];
  
  return min;
}

unsigned char find_maximum(unsigned char *a, const unsigned int sz) {
	int i;
  unsigned char max = a[0];
	
  if( sz < 2) return max;
	
  for (i = 1; i != sz; i++)
		if (a[i] > max)
			max = a[i];
  
  return max;
}

void minimum_left(unsigned char *a, const unsigned int sz) {
	int i;
  unsigned char min, min_old;
  
  if( sz < 2 ) return;
  
  min = a[sz-1];
  
	for (i = 0; i < sz-1; i++) {
		if (a[i] < min) {
      min_old = min;
			min = a[i];
      a[i] = min_old;
    }
  }
  a[sz-1] = min;
}

void maximum_right(unsigned char *a, const unsigned int sz) {
	int i;
  unsigned char max = a[0], max_old;
	
	for (i = 1; i != sz; i++) {
		if (a[i] > max) {
      max_old = max;
			max = a[i];
      a[i] = max_old;
    }
  }
  a[0] = max;
}

void sort_array(unsigned char *a, const unsigned int sz) {
  int p, q;
  if (sz < 2) return;
  p = 0;
  q = sz;
  while(p < q) {
  minimum_left((a+p),q-p);
  maximum_right((a+p),q-p);
  p++;
  q--;
  }
}

void print_array(unsigned char *a, const unsigned int sz) {
#ifdef VERBOSE  
	int k;
  for(k=0; k != sz; k++) {
    PRINTF("%d  ", a[k]);
    if(k!=0 && !(k%15) ) PRINTF("\n");
  }
  PRINTF("\n\n");
#endif
}

unsigned char find_mean(unsigned char *a, const unsigned int sz) {
  unsigned int acc = 0;
  int i;
  
  for(i=0; i!=sz; i++) acc += a[i];  
  
  return acc/sz;
}

unsigned char find_median(unsigned char *a, const unsigned int sz) {
  int mid = sz / 2;
  sort_array(a,sz);
  return sz % 2 == 0 ? (a[mid]+a[mid-1]) / 2 : a[mid];
}

void print_statistics(unsigned char *a, const unsigned int sz) {
#ifdef VERBOSE  
	PRINTF("minimum = %d\n", find_minimum(a,sz));
  PRINTF("maximum = %d\n", find_maximum(a,sz));
  PRINTF("mean = %d\n", find_mean(a,sz));
  PRINTF("median = %d\n", find_median(a,sz));
#endif
}