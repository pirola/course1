include sources.mk

BASENAME=course1
TARGET =$(BASENAME).out
CPPFLAGS = -D$(PLATFORM) -DCOURSE1
CFLAGS = -Wall -Werror -g -O0 -std=c99

ifeq ($(VERBOSE),1)
CPPFLAGS += -DVERBOSE
endif

##################################
# For MSP432 platform:
ifeq ($(PLATFORM),MSP432)
# Architecture Specific Flags
CPU =cortex-m4
ARCH = thumb
MARCH= armv7e-m
FLOAT= hard
FPU=fpv4-sp-d16
SPECS = nosys.specs

# Platform Specific Flags
LINKER_FILE =./msp432p401r.lds


IDIR+=$(IDIR_MSP)
SRCS+=$(SRCS_MSP)

# Compile Defines
CC = arm-none-eabi-gcc
LD = arm-none-eabi-ld
LDFLAGS = -Wl,-Map=$(BASENAME).map -T $(LINKER_FILE)
CFLAGS += -mcpu=$(CPU) -m$(ARCH) -march=$(MARCH) \
					-mfloat-abi=$(FLOAT) -mfpu=$(FPU) --specs=$(SPECS)
endif
##################################
# For HOST platform:
ifeq ($(PLATFORM),HOST)

# Compile Defines
CC = gcc
LD = ld
endif

OBJS:=$(SRCS:.c=.o)


%.i: %.c
	$(CC) -E $(CPPFLAGS) $(IDIR) -o $@ -c $<

%.asm: %.c
	$(CC) -S $(CPPFLAGS) $(CFLAGS) $(IDIR) -o $@ -c $<

%.o: %.c
	$(CC) $(CPPFLAGS) $(CFLAGS) $(IDIR) -o $@ -c $<

.PHONY: compile-all
compile-all: $(SRCS)
	$(CC) $(CPPFLAGS) $(CFLAGS) $(IDIR) -c $^

.PHONY: build
build: $(TARGET)

$(TARGET): $(OBJS)
	$(CC) $(CPPFLAGS) $(CFLAGS) $(LDFLAGS) $(IDIR) -o $(TARGET) $^


.PHONY: clean
clean:
	rm -f $(OBJS) $(SRCS_MSP:.c=.o) $(TARGET) $(BASENAME).map 