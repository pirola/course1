/******************************************************************************
 * Copyright (C) 2017 by Alex Fosdick - University of Colorado
 *
 * Redistribution, modification or use of this software in source or binary
 * forms is permitted as long as the files maintain this copyright. Users are 
 * permitted to modify this and use it to learn about the field of embedded
 * software. Alex Fosdick and the University of Colorado are not liable for any
 * misuse of this material. 
 *
 *****************************************************************************/
/**
 * @file stats.h 
 * @brief Definitions for stats.c source f ile
 *
 * Definitions used in stats.c source file
 *
 * @author Lucas Pirola
 * @date 22/Feb/2018
 *
 */
#ifndef __STATS_H__
#define __STATS_H__

/* Add Your Declarations and Function Comments here */ 

/**
 * @brief Returns the minimum value of an array
 *
 * This function receives an array of unsigned chars, its size,
 * and returns an unsigned char with the MINIMUM value of the array.
 *
 * @param a The array to be used
 * @param sz The size of the array
 *
 * @return The mininum value of this array
 */
 unsigned char find_minimum(unsigned char *a, const unsigned int sz);


/**
 * @brief Returns the maximum value of an array
 *
 * This function receives an array of unsigned chars, its size,
 * and returns an unsigned char with the MAXIMUM value of the array.
 *
 * @param a The array to be used
 * @param sz The size of the array
 *
 * @return The mininum value of this array
 */unsigned char find_maximum(unsigned char *a, const unsigned int sz);

 
 /**
 * @brief Puts MINIMUM value at the end of an array
 *
 * This function receives an array of unsigned chars, its size,
 * and permutes the array's elements until the last array's element
 * is the MINIMUM value of the entire array.
 *
 * @param a The array to be used
 * @param sz The size of the array
 *
 * @return void
 */
void minimum_left(unsigned char *a, const unsigned int sz);


 /**
 * @brief Puts MAXIMUM value at the beginning of an array
 *
 * This function receives an array of unsigned chars, its size,
 * and permutes the array's elements until the first array's element
 * is the MAXIMUM value of the entire array.
 *
 * @param a The array to be used
 * @param sz The size of the array
 *
 * @return void
 */
void maximum_right(unsigned char *a, const unsigned int sz);


 /**
 * @brief Sort an array, beginning with the biggest value
 *
 * This function receives an array of unsigned chars, its size,
 * and permutes the array's elements until the it is sorted,
 * being the first element the biggest one and the last element
 * the smallest value.
 *
 * @param a The array to be used
 * @param sz The size of the array
 *
 * @return void
 */
void sort_array(unsigned char *a, const unsigned int sz);


 /**
 * @brief Prints an array, 15 positions per line
 *
 * This function receives an array of unsigned chars, its size,
 * and prints it to the standard output.  Line breaks occur every
 * 15th element printed.
 *
 * @param a The array to be used
 * @param sz The size of the array
 *
 * @return void
 */
void print_array(unsigned char *a, const unsigned int sz);


/**
 * @brief Returns the MEAN value of an array
 *
 * This function receives an array of unsigned chars, its size,
 * and returns the MEAN value of all elements in it.
 *
 * @param a The array to be used
 * @param sz The size of the array
 *
 * @return The MEAN value of this array
 */
unsigned char find_mean(unsigned char *a, const unsigned int sz);


/**
 * @brief Returns the MEDIAN value of an array
 *
 * This function receives an array of unsigned chars, its size,
 * and returns the MEDIAN value of all elements in it.
 *
 * @param a The array to be used
 * @param sz The size of the array
 *
 * @return The MEDIAN value of this array
 */
unsigned char find_median(unsigned char *a, const unsigned int sz);


/**
 * @brief Prints statistics of an array
 *
 * This function receives an array of unsigned chars, its size,
 * and prints to the standard output the minimum, the maximum,
 * the mean and the median values of this same array.
 *
 * @param a The array to be used
 * @param sz The size of the array
 *
 * @return void
 */
void print_statistics(unsigned char *a, const unsigned int sz);


#endif /* __STATS_H__ */
